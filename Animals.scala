

// abstract class: cannot be instantiated
abstract class Animal {
  // abstract method, means: without implementation
  def speak: String
  def speakTwice : String = speak + speak
  def kingdom: String = "Animalia"
  def name : String
  def describe : String = "The " + name + " says " + speak

}

// subclass of animal
class Dog() extends Animal {
  def name : String = "Dog"
  def speak : String = "Woof woof!"

}

class GoldenRetriever extends Dog() {
  override def speak : String  = super.speak + " old chap "
}



class Cat()  extends Animal {
  def name  :String = "Cat"
  def speak : String = "Miauw!!"
}

class Parrot(val line : String) extends Animal {
  def name : String = "Parrot"
  def speak : String = line

   override def describe: String = "Its a parrot"

}

// Animal is open for extension: anyone can add an animal

object Animals {
  // polymorphic code: this function works for any subtype of animal
  def speakTwice(animal: Animal): String =
    animal.speak + animal.speak
}

