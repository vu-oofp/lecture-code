import scala.annotation.tailrec

sealed abstract class Lst[A] {
  // l ++ r = replace NNil with r in l
  def ++(rhs : Lst[A]) : Lst[A] = this match {
    case NNil() => rhs
    // h = 1, t = Cons(2,Cons(3,NNil)) , rhs = Cons(4,Cons(5,NNill))
    // Cons(1,Cons(2,Cons(3,Cons(4,Cons(5,NNil())))))))
    case Cons(h,t) => Cons(h,t ++ rhs)
  }

  def foldRight[B](z : B)(f : (A,B) => B) : B =
    this match {
      case NNil() => z
      case Cons(h,t) => f(h,t.foldRight(z)(f))
    }

  final def foldLeft[B](z : B) (f : (B,A) => B) : B = {

    this match {
      case NNil() => z
      case Cons(h,t) => t.foldLeft(f(z,h))(f)
    }
    /* imperative version :
    var res = z
    var cur = this
    while(cur != NNil()) {
      cur match {
        case Cons(h,t) => {
          res = f(res,h)
          cur = t
        }
        case _ => throw new Error("Warning: Universe unstable")
      }
    }
    res
     */


  }

}

case class NNil[A]() extends Lst[A]
case class Cons[A](head : A, tail : Lst[A]) extends Lst[A]

object Lst {
  def until(n : Int) : Lst[Int] = {
    var res : Lst[Int] = NNil()
    var nv = n
    while(nv >= 0) {
      res = Cons(nv,res)
      nv-=1
    }
    res
  }

}
