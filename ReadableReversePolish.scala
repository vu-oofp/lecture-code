
import collection.mutable.Stack
import scala.collection.immutable.List


// calculates expressions in
// "Reverse polish notation"
// see Computerphile https://www.youtube.com/watch?v=7ha78yWRDlE&t=623s
// for an intro on reverse polish
object ReversePolish {


  def isNumber(s: String): Boolean = {
    if(s.isEmpty) return false
    for(c <- s.toCharArray) {
      if(!c.isDigit) return false
    }
    return true
  }

  // examples:
  //  1 2 + -> 3
  //  4 3 * 1 + -> 13
  //  2 3 * 3 2 * * 1 + 37
  def calculate(expression : String) : Int = {
    val s : Stack[Int] = new Stack()
    for(el <- expression.split(" ")) {
      if(isOperator(el)) {
        val rhs = s.pop
        val lhs = s.pop
        val res = operatorByName(el, lhs, rhs)
        s.push(res)
      } else if(isNumber(el)) s.push(el.toInt)
      else throw new Error("Unknown expression element " + el)
    }
    s.top
  }

  private def operatorByName(opName: String, lhs: Int, rhs: Int) = {
    if (opName == "+") lhs + rhs
    else if (opName == "*") lhs * rhs
    else lhs / rhs
  }

  private def isOperator(s: String) = s == "+" || s == "*" || s == "/"
}
