
import collection.mutable.Stack

abstract class Expression {
  def value : Int
  def describe : String =
    "The value of " + toString + " is " + value.toString
}

case class Constant(n : Int) extends Expression {
  override def value: Int = n

  override def toString: String = n.toString
}

case class Operator(lhs : Expression, operatorName : String,rhs : Expression ) extends  Expression {
  override def value: Int = operatorByName(operatorName, lhs.value, rhs.value)

  private def operatorByName(opName: String, lhs: Int, rhs: Int) = {
    ReversePolish.operatorsByName(opName)(lhs,rhs)
  }

  override def toString: String = "(" + lhs.toString + operatorName + rhs.toString + ")"
}

object ReversePolish {
  
  val operatorsByName : Map[String, (Int,Int) => Int] =
    Map(("+", _ + _ ) ,
        ("-" , _ - _) ,
      ("*", _ * _) ,
      ("/" , _ / _))

  def isNumber(s: String): Boolean = {
    if(s.isEmpty) return false
    for(c <- s.toCharArray) {
      if(!c.isDigit) return false
    }
    return true
  }



  // examples:
  //  1 2 + -> 3
  //  4 3 * 1 + -> 13
  //  2 3 * 3 2 * * 1 + 37
  def reversePolishToExpression(expression : String) : Expression = {
    val s : Stack[Expression] = new Stack()
    for(el <- expression.split(" ")) {
      if(isOperator(el)) {
        val rhs = s.pop
        val lhs = s.pop
        val res = Operator(lhs,el, rhs)
        s.push(res)
      } else if(isNumber(el)) s.push(Constant(el.toInt))
      else throw new Error("Unknown expression element " + el)
    }
    s.top
  }
  
  private def isOperator(s: String) = operatorsByName.contains(s)
}



