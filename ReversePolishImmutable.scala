
import collection.mutable.Stack

object ReversePolish {


  def isNumber(s: String): Boolean = {
    if(s.isEmpty) return false
    for(c <- s.toCharArray) {
      if(!c.isDigit) return false
    }
    return true
  }

  def calculate(expression : String) : Int = {
    var s = SStack[Int]()
    for(el <- expression.split(" ")) {
      if(el == "+" || el == "*" || el == "/") {
        val rhs = s.top
        s = s.pop
        val lhs = s.top
        s = s.pop
        val res =
          if(el == "+") lhs + rhs
          else if(el == "*") lhs * rhs
          else lhs / rhs
        s = s.push(res)
      } else if(isNumber(el)) s = s.push(el.toInt)
      else throw new Error("Unknown expression element " + el)
    }
    s.top
  }
}

case class SStack[A](l : List[A]) {
  def isEmpty : Boolean = l.isEmpty
  def top : A = l.head
  def pop : SStack[A] = SStack(l.tail)
  def push(a : A) : SStack[A] = SStack(a :: l)
}

object SStack{
  def apply[A]() : SStack[A] = SStack(List())
}