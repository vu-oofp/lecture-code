

import collection.mutable.Stack

abstract class Expression

case class Constant(i : Int) extends  Expression {
  override def toString: String =i.toString
}

case class Variable(s : String) extends Expression {
  override def toString: String = s
}

case class Operator(lhs : Expression, operatorName : String,rhs : Expression ) extends  Expression {
  override def toString: String = "(" + lhs.toString + operatorName + rhs.toString + ")"
}

object PatternMatchReversePolish {

  // bottom-up simplify
  // rules
  // 0 + a -> a
  // a + 0 -> a
  // a + a -> 2 * a

  def simplify(exp : Expression) : Expression = {
    exp match {
      case Operator(lhs, op, rhs) =>
        // simplify sub-expression first, as they
        // might change through simplification
        val lhsp = simplify(lhs)
        val rhsp = simplify(rhs)
        (lhsp, op, rhsp) match {
          case (Constant(0), "+", _) => rhsp
          case (_, "+", Constant(0)) => lhsp
          case (a, "+", b) if a == b => Operator(Constant(2), "*", a)
          case _ => Operator(lhsp, op, rhsp)
        }
      case _ => exp
    }
  }



  def isNumber(s: String): Boolean = {
    if(s.isEmpty) return false
    for(c <- s.toCharArray) {
      if(!c.isDigit) return false
    }
    return true
  }



  // examples:
  //  1 2 + -> 3
  //  4 3 * 1 + -> 13
  //  2 3 * 3 2 * * 1 + 37
  def reversePolishToExpression(expression : String) : Expression = {
    val s : Stack[Expression] = new Stack()
    for(el <- expression.split(" ")) {
      if(isOperator(el)) {
        val rhs = s.pop
        val lhs = s.pop
        val res = Operator(lhs,el, rhs)
        s.push(res)
      } else if(isNumber(el)) s.push(Constant(el.toInt))
      else s.push(Variable(el))
    }
    s.top
  }

  private def isOperator(s: String) = s == "+" || s == "*" || s == "/"

  def simplifyReversePolish(expression : String)  : String =
    simplify(reversePolishToExpression(expression)).toString

  // 0 + a + 0 -> a
  def example1 : String = simplifyReversePolish("0 a 0 + +" )

  // a + a -> 2 * a
  def example2 : String = simplifyReversePolish("a a +")

  // (a + b) + (a + b) -> 2 * (a + b)
  def example3 : String = simplifyReversePolish("a b + a b + +")

  // 0 + (b x c + b x c) + 0 -> 2 * (b * c)
  def example4 : String = simplifyReversePolish("0 b c * b c * + + 0 + ")
}
