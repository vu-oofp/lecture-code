abstract class Expression {
  def eval(bindings : Map[String,Int]) : Int
}

case class Const(i : Int) extends  Expression {
  override def eval(bindings: Map[String, Int]): Int = i
}
case class Var(s : String) extends Expression {
  override def eval(bindings: Map[String, Int]): Int = bindings(s)
}

case class Negate(arg : Expression) extends Expression {
  override def eval(bindings: Map[String, Int]): Int = -arg.eval(bindings)
}

case class Operator(lhs : Expression, operatorName : String,rhs : Expression ) extends  Expression {
  override def eval(bindings: Map[String, Int]): Int = {
    val l = lhs.eval(bindings)
    val r = rhs.eval(bindings)
    PatternMatch.operatorByName(l,operatorName,r)
  }
}



// examples
// x
object PatternMatch {

  def operatorByName(l : Int, name : String, r : Int) : Int = {
    name match {
      case "+" => l + r
      case "-" => l - r
      case "*" => l * r
      case "/" => l / r
    }
  }

  def eval(bindings : Map[String, Int], exp : Expression) : Int =
    exp match {
      case Const(i) => i
      case Var(s) => bindings(s)
      case Negate(arg) => -eval(bindings, arg)
      case Operator(lhs, op , rhs) =>
        operatorByName(eval(bindings,lhs), op, eval(bindings, rhs))
    }

  // rules :
  // -(-e) 	=> e
  // e + 0 => e
  // e * 1 => e
  def simplify(exp : Expression) : Expression =
    exp match {
      case Negate(Negate(e)) => simplify(e)
      case Operator(e, "+" , Const(0)) => simplify(e)
      case Operator(e, "*" , Const(1)) => simplify(e)
      case Negate(e) => Negate(simplify(e))
      case Operator(l,op,r) => Operator(simplify(l), op, simplify(r))
      case _ => exp
    }


  // -(-3) -> 3
  val simpEx1 = simplify(Negate(Negate(Const(3))))
  // -(-3) + 0 -> 3
  val simpEx2 = simplify(Operator(Negate(Negate(Const(3))), "+", Const(0)))
  // ((x * x) * 1) / ((y + 3) + 0) -> (x * x) / (y + 3)
  val simpEx3 = simplify(
    Operator(
     Operator(Operator(Var("x"), "*" , Var("x")), "*", Const(1)),
      "/",
      Operator(Operator(Var("y"), "+" , Const(3)), "+", Const(0)),
    )
  )


  // x
  val ex1 = Var("x")

  val evalEx1 = ex1.eval(Map("x" -> 1)) // gives 1

  // 2
  val ex2 = Const(2)

  val evalEx2 = ex2.eval(Map())         // gives 2

  // -(-x)
  val ex3 = Negate(Negate(Var("x")))

  val evalEx3 = ex3.eval(Map("x" -> 3)) // gives 3

  // 2 * x
  val ex4 = Operator(Const(2), "*", Var("x"))

  val evalEx4 = ex4.eval(Map("x" -> 3)) // gives 6

  // (x / y) * 2 + 1
  val ex5 =
    Operator(
      Operator(
            Operator(Var("x"),"/",Var("y")),
            "*", Const(2)),
      "+", Const(1))

  val evalEx5 = ex5.eval(Map("x"-> 6, "y" -> 3)) // gives 5

}
