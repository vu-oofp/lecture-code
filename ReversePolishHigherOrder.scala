
object ReversePolish {

  val operatorsByName : Map[String, (Int,Int) => Int] =
    Map(("+", _ + _ ) ,
        ("-" , _ - _) ,
      ("*", _ * _) ,
      ("/" , _ / _))

  def isNumber(s: String): Boolean = s.toSeq.forall(_.isDigit)

  def handleElement(initS : SStack[Int], el : String) : SStack[Int] = {
    var s = initS
    if(isOperator(el)) {
      val rhs = s.top
      s = s.pop
      val lhs = s.top
      s = s.pop
      val res = operatorsByName(el)(lhs,rhs)
      s.push(res)
    } else if(isNumber(el)) s.push(el.toInt)
    else throw new Error("Unknown expression element " + el)
  }

  def calculate(expression : String) : Int =
    expression.split(" ").foldLeft( SStack[Int]())(handleElement).top 


  private def isOperator(s: String) = operatorsByName.contains(s)
}




case class SStack[A](l : List[A]) {
  def isEmpty : Boolean = l.isEmpty
  def top : A = l.head
  def pop : SStack[A] = SStack(l.tail)
  def push(a : A) : SStack[A] = SStack(a :: l)
}

object SStack{
  def apply[A]() : SStack[A] = SStack(List())
}
